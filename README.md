# Black Hole Accretion Disk Impact Model

This reposity contains a Cython script (burst_model.pyx), which can be
used to compute the spectral evolution of a black hole accretion disk
impact. The details of the model are available in the following publication: 

> **P. Pihajoki, Monthly Notices of the Royal Astronomical Society 2016 457 (2): 1145-1161**

## Prerequisites

The following Python packages need to be installed on the system
(parenthesis indicate the package version used to test the code):

* Cython (0.20.1post0)
* NumPy (1.9.2)
* SciPy (0.15.1)

## Usage instructions

Note: Unless otherwise specified, all quantities are in cgs units.

The main entry point of the model is the `model` function, defined as:
```python
def model(np.ndarray[DTYPE_t, ndim=1] ts_in, DTYPE_t z, DTYPE_t luminosity_distance, np.ndarray[DTYPE_t,ndim=1] spectrum_bins, *params, use_obs_frame=True)
```

The named arguments have the following meaning:

* `ts_in`:                  The *observed* times at which the light curve is evaluated [Julian years]
* `z`:                      Cosmological redshift [-]
* `luminosity_distance`:    Luminosity distance [cm]
* `spectrum_bins`:          Frequency values at which the observed spectrum is evaluated [Hz]

The `*params` tuple is used to input the model initial conditions: 

* `R_0`:                    The initial (t=0) radius [cm]
* `T_0`:                    The initial equilibrium temperature [K]
* `rho_0`:                  The initial density of the shocked gas [g/cm^3]
* `beta_0`:                 The initial expansion velocity / c [-]

Finally, the keyword arguments are used for optional settings:

* `use_obs_frame`:         If True, calculate model quantities (luminosity, temperature etc.) as seen by the observer for a photospheric point at theta=0. if False, calculate assuming ts_in refers to photospheric rest frame. (default True)

Also provided is the `calculate_impact` function, defined as:
```python
def calculate_impact(r10, h15, n14, M8, incp, molecular_mass=0.5)
```

This function takes the following arguments:

* `r10`: Impact distance in units of 10 Schwarzschild radii of the primary black hole
* `h15`: The local accretion disk semiheight in units of 10^15 cm
* `n14`: Local number density in units of 10^14 cm^-3
* `M8`: Impactor mass in units of 10^8 Solar masses
* `incp`: The inclination parameter, sqrt(1-sin(inclination))
* `molecular_mass`: Local accretion disk molecular mass. Default value 0.5.

## Outputs

The `model` function returns a Python dict structure with the following fields:

* `spectra`: An array with shape `(len(ts_in), len(spectrum_bins)`, containing the calculated radiant spectral flux at each time and given frequency. In units of erg/(s Hz cm^2). Always computed in the observer rest frame.
* `time`: Input times in seconds.
* `output_time`: Times at which the outputs have been calculated. See `use_obs_frame` parameter above.
* `L`: Surface luminosity. In units of erg/s. 
* `Ld`: Photon diffusion contribution to luminosity. In units of erg/s.
* `Lt`: Optically thin contribution to luminosity. In units of erg/s.
* `xe`: Relative radius of photosphere using effective optical depth. Dimensionless.
* `xs`: Relative radius of photosphere using electron scattering optical depth. Dimensionless.
* `xp`: Relative radius of photosphere. Dimensionless.
* `phi`: The phi function originally defined in Arnett (1980). Dimensionless.
* `xi`: Expansion factor R(t)/R0. Dimensionless.
* `Teff`: Effective temperature of the photosphere. In units of K.
* `T`: Temperature of the outflow. In units of K.
* `R`: Fiducial radius of the outflow. In units of cm.
* `z`: Input redshift.
* `DL`: Input luminosity distance. In units of cm.
* `R0`: Input initial radius. In units of cm.
* `T0`: Input initial temperature. In units of K.
* `rho0`: Input initial density. In units of g/cm^3.
* `beta0`: Input initial expansion velocity. In units of c.
* `taup`: Optical depth of the photosphere. Default 2/3.
* `alpha`: The alpha parameter. Default pi^2.
* `M0`: Initial mass of the outflow sphere. In units of g.
* `td`: Initial diffusion time-scale. In units of s.
* `th`: Initial hydrodynamic time-scale. In units of s.
* `taus0`: Initial optical depth due to electron scattering. Dimensionless.
* `taua0`: Initial optical depth from Kramers' law. Dimensionless.
* `ET0`: Initial radiative energy of the outflow. In units of erg.

## Sample run

```python
    import matplotlib.pyplot as plt
    import numpy as np

    # for directly importing Cython files, pyximport must be used
    import pyximport; pyximport.install()
    import burst_model

    # set parameters of the binary and the accretion disk
    z = 0.5
    lum_dist = 7.953e27 # in cm, using Planck2013 cosmology
    M8  = 1.0
    r10 = 1.0
    h15 = 1.0
    n14 = 1.0
    incp = 1.0
    # set beta equal to impact velocity
    beta = 1/np.sqrt(10.0)*r10**(-1./2)

    # convert to initial outflow values
    (T0, R0, rho0) = burst_model.calculate_impact(r10, h15, n14, M8, incp)

    # evaluate at 100 times divided evenly over 0.5 years
    times = np.linspace(0, 0.5, 100)

    # get observed flux at frequency corresponding to wavelength of 500 nm
    freq = 6e14

    # run model
    params = (R0, T0, rho0, beta)
    model = burst_model.model(times, z, lum_dist, np.array([freq]), *params)
    plt.plot(times, model['spectra'])
    plt.show()
```

Running this code should produce the following image.

![sample_image](sample_image.png)

## License

The code is released under the 3-clause ("Revised", "Modified") BSD License.
See the LICENSE file.