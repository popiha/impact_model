## A model of a relativistic outflow from an accretion disk impact

# Copyright (c) 2015, Pauli Pihajoki and the University of Helsinki
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the University of Helsinki nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## The Model:
#
# input (arguments):
#   ts_in:                  the _observed_ times at which the light curve is to be evaluated [Julian years]
#   z:                      cosmological redshift [-]
#   luminosity_distance:    luminosity distance [cm]
#   spectrum_bins:          frequency values at which the we evaluate the observed spectrum [Hz]
#
# input (*params):
#   R_0:                    the initial (t=0) radius [cm]
#   T_0:                    the initial equilibrium temperature [K]
#   rho_0:                  the initial density of the shocked gas [g/cm^3]
#   beta_0:                 the initial expansion velocity / c [-]
#
# input (keyword arguments):
#   use_obs_frame:         if True, calculate model quantities (luminosity,
#                          temperature etc.) as seen by the observer for a photospheric point at
#                          theta=0. if False, calculate assuming ts_in refers to photospheric rest
#                          frame. (default True)
#
# output:
#   calculated spectra and model physical quantities for each time in a python dict
#   data structure

# dependencies:
#   NumPy
#   SciPy

from __future__ import division

import numpy as np
cimport numpy as np

import scipy as sp
import scipy.integrate as integ
import scipy.optimize
import scipy.misc

cimport cython

# datatype of the arrays
DTYPE = np.double
ctypedef np.double_t DTYPE_t

# replace numpy min/max
cdef inline double dbl_max(double a, double b): return a if a >= b else b
cdef inline double dbl_min(double a, double b): return a if a <= b else b

# use CODATA 2010 values for physical constants, and convert to cgs units
cdef DTYPE_t c        = 1e2 * 2.99792458e8
cdef DTYPE_t sigma_sb = 1e3 * 5.670373e-8
cdef DTYPE_t m_p      = 1e3 * 1.672621777e-27
cdef DTYPE_t h        = 1e7 * 6.62606957e-34
cdef DTYPE_t k_B      = 1e7 * 1.3806488e-23 
#
# global options

# spectrum integration tolerances
cdef DTYPE_t si_tolrel = 1e-3
cdef DTYPE_t si_tolabs = 0

#
# physical constants & auxiliary functions

# seconds in a julian year
cdef DTYPE_t secs_in_julyr = 365.25*86400

# radiation constant
cdef DTYPE_t a_rad = 4*sigma_sb/c

# Thomson scattering cross section [cm^2]
cdef DTYPE_t sigma_T = 6.65e-25

# mean molecular mass for fully ionized hydrogen gas
cdef DTYPE_t mu = 0.5

# Thomson opacity
cdef DTYPE_t kappa_T = sigma_T / (mu * m_p)

# Planck's radiation law in frequency form
def planck(DTYPE_t nu, DTYPE_t T):
    # handle corner cases
    if T <= 0:
        return 0.0
    return 2 * h * nu**3 / c**2 * (np.exp(h*nu/(k_B*T)) - 1)**(-1)

#
# utility for converting a binary orbital and accretion disk parameters
# to the physical parameters of the shock impact region. uses the
# approximations in the paper section 2.
# arguments:
#   M8   - normalized impactor mass
#   r10  - normalized impact distance
#   incp - normalized impactor inclination parameter, sqrt(1 - sin(theta_i))
#   h15  - normalized accretion disk semiheight
#   n14  - normalized accretion disk number density
def calculate_impact(r10, h15, n14, M8, incp, molecular_mass=mu):
    Teq = 1.5e6 * n14**(1./4) * r10**(-1./4) * incp**(1./2)
    R0 = 2.1e14 * h15**(1./3) * M8**(2./3) * r10**(2./3) * incp**(4./3)

    # rho = mu * compression ratio * proton mass * number density
    rho0 =  molecular_mass * 7 * m_p * 1e14 * n14
    return (Teq, R0, rho0)

#
# the model, with arguments specfied in the header
@cython.boundscheck(False)
def model(np.ndarray[DTYPE_t, ndim=1] ts_in, DTYPE_t z, DTYPE_t luminosity_distance, np.ndarray[DTYPE_t,ndim=1] spectrum_bins, 
        *params, use_obs_frame=True):

    assert ts_in.dtype == DTYPE

    # unpack parameters
    cdef DTYPE_t R0      = params[0]
    cdef DTYPE_t T0      = params[1]
    cdef DTYPE_t rho0    = params[2]
    cdef DTYPE_t beta0   = params[3]

    # photospheric depth
    cdef DTYPE_t taup = 2./3

    # derived values
    cdef DTYPE_t alpha = np.pi**2                                # eigenvalue of the radiative zero solution
    cdef DTYPE_t M0    = 4*np.pi/3*R0**3 * rho0                     # total mass
    cdef DTYPE_t td    = 3*R0**2*kappa_T*rho0/(c*alpha)         # diffusion time scale
    cdef DTYPE_t th    = R0/(c*beta0)                           # hydrodynamical time scale
    cdef DTYPE_t taus0 = R0*rho0*kappa_T                         # initial scattering optical depth
    cdef DTYPE_t taua0 = R0*rho0*8e25*2*rho0*T0**(-7./2)         # initial absorption optical depth, pure hydrogen

    # convert input from years to seconds
    cdef np.ndarray tssec = ts_in * secs_in_julyr

    #
    # print input and derived quantities?
    if 0:
        print "R0    ", R0    
        print "T0    ", T0    
        print "rho0  ", rho0  
        print "beta0 ", beta0 
        print "taup  ", taup  
        print "alpha ", alpha 
        print "M0    ", M0    
        print "td    ", td    
        print "th    ", th    
        print "taus0 ", taus0 
        print "taua0 ", taua0 


    # radius as a function of time
    def Rfunc(t):
        if t < -th:
            return 0.0
        else:
            return R0*(1+t/th)

    # radial velocity
    def Rdot(DTYPE_t t):
        if t < -th:
            return 0.0
        else:
            return beta0

    # solution for the phi function, giving time evolution of radiative energy
    def phi(DTYPE_t t):
        # if diffusion time ~ 0 or R0~0, phi will be ~0
        if td <= 0 or R0 <= 0:
            return 0.0
        # R integral
        #cdef DTYPE_t rint = 0.0 if t < 0 else R0*t + 0.5 * c*beta0*t**2
        #cdef DTYPE_t retval = np.exp(-alpha/(td*R0) * rint)
        cdef DTYPE_t rint = 0.0 if t < 0 else th/2*( (1+t/th)**2 - 1)
        #cdef DTYPE_t retval = np.exp(-alpha/td * rint)
        cdef DTYPE_t retval = np.exp(-1/td * rint)
        if retval < 0:
            return 0.0
        if retval > 1:
            return 1.0
        return retval

    # solution for the psi function, giving spatial distribution of radiative
    # energy
    def psi(DTYPE_t x):
        # this is for the homogeneous density & zero-radiation case.
        # note, numpy defines sinc(x) = sin(pi x)/(pi x)
        #return np.sinc(x/np.pi*np.sqrt(alpha))

        # this is for the homogenous density & temperature -profile case
        return 1.0

    # the initial radiation content forms (i.e. "heating" of the radiation) in
    # the shock heated matter by the _matter cooling_, given by the relevant
    # cooling timescale, which we have chosen to be the free-free timescale.
    # in practice, this makes very little difference, since the timescales are
    # very small compared to the outburst length
    def heating(DTYPE_t t):
        cdef DTYPE_t tff = 160./(rho0/(mu*m_p))
        cdef np.ndarray mult = np.exp(t/tff)
        if mult > 1.0:
            return 1.0
        else:
            return mult

    # the factors ET & IT within radius
    def IT(DTYPE_t x):
        # integral of x'^2 psi(x') from x'=0 to x'=x.
        # this is for the homogeneous density & zero-radiation case
        #arg=np.pi*x
        #return (np.sin(arg) - np.pi*x*np.cos(arg))/np.pi**3

        # this is for the homogenous density & temperature -profile case
        return x**3/3.0

    def ET(DTYPE_t x, DTYPE_t t):
        # for t < 0, the radiation energy content grows from zero, using the
        # appropriate heating timescale. however, as per paper, this is
        # very quick, so we use zero for t < 0
        if t <= 0:
            return 0.0
        cdef DTYPE_t R = Rfunc(t)
        return 4*np.pi * R0**3 * a_rad*T0**4 * phi(t) * R0/R * IT(x) # * heating(t)

    # position of the scattering photosphere
    def xs(DTYPE_t t):
        cdef DTYPE_t R = Rfunc(t)
        if R0 <= 0:
            return 0.0
        cdef DTYPE_t xi = R/R0
        # calculate position of optical thinness
        cdef DTYPE_t res = 1 - taup/taus0 * xi**2
        
        return dbl_min(dbl_max(0,res), 1)

    # position of the effective optical thickness photosphere
    def xe(DTYPE_t t):
        # for sufficiently large negative values of t, R = 0 and
        # 1/xi -> inf. in these cases, xa should be zero.

        cdef DTYPE_t R = Rfunc(t)
        cdef DTYPE_t vel = Rdot(t)
        if R0 <= 0:
            return 0.0
        cdef DTYPE_t xi = R/R0
        cdef DTYPE_t beta = vel/c
        cdef DTYPE_t ph = phi(t)

        # if necessary for numerical computation, ignore floating point accuracy warnings
        #original_settings = np.geterr()
        #np.seterr(all='ignore')

        if xi <= 0 or beta <= 0 or R <= 0 or ph <= 0:
            return 0.0

        # use the exact Shibata (2014) result
        # gives shibata optical thickness at given x
        def shibata_te(DTYPE_t x):
            cdef DTYPE_t gamma = 1/np.sqrt(1-beta**2)
            # optical depths for x = 0, multiply by (1-x) to get at arbitrary x,
            # valid only for constant density and temperature
            cdef DTYPE_t taus = taus0 * xi**(-2)
            cdef DTYPE_t taua = taua0 * xi**(-3./2) * ph**(-7./8)
            cdef DTYPE_t taueff = np.sqrt((1-x)**2 * taua * (taua+taus))/(gamma*(1-beta)) * \
                (2./3*gamma**2*(beta**2+3) + (gamma*beta)**2 * taus/taua)**(-1/2.)
            #print "x", x, "gamma", gamma, "taus", taus, "taua", taua, "taueff", taueff
            return taueff

        # we need to solve for shibata_te(x_p) = taup. check first that the bubble isn't optically thin.
        if shibata_te(0) < taup:
            return 0.0

        # solve numerically
        #cdef DTYPE_t res = scipy.optimize.brentq(lambda x: shibata_te(x) - taup, 0, 1)

        # analytic formula, valid for homogenous case
        cdef DTYPE_t res = 1 - taup/shibata_te(0)

        # restore warning settings
        #np.seterr(**original_settings)

        # furthermore, spherical symmetry requires x >= 0, and the definition of x requires x <= 1
        res = dbl_min(dbl_max(0, res), 1)
        return res

    # we need the minimum of x_p in supplied time interval, but only if there
    # actually is a time _interval_
    cdef DTYPE_t xp_min_time
    cdef DTYPE_t xp_min
    def opt_callback(t):
        print "t", t
    def opt_obj(t):
        return dbl_max(xs(t), xe(t))
    
    # find where to start
    cdef unsigned int i
    cdef unsigned int min_i = 0
    cdef DTYPE_t opt_val = opt_obj(tssec[0])
    if len(tssec) > 1:
        for i in xrange(len(tssec)):
            if opt_obj(tssec[i]) < opt_val:
                min_i = i
                opt_val = opt_obj(tssec[i])
        #print "opt_val", opt_val, "min_i", min_i, "opt_t", tssec[min_i]

        optresult = scipy.optimize.minimize(opt_obj, tssec[min_i], 
                bounds=[(tssec[0],tssec[-1])]
                )
        #print "xp_min optimization", optresult
        xp_min_time = optresult.x
        xp_min      = optresult.fun
    else:
        xp_min_time = tssec[0]
        xp_min      = opt_obj(tssec[0])

    # the photosphere = max(xs, xe)
    def xp(DTYPE_t t):
        return dbl_max(xs(t), xe(t))

    # the extent of that has not been optically thin at any point
    def xthick(DTYPE_t t):
        cdef DTYPE_t x = xp(t)
        if t > xp_min_time:
            x = dbl_min(xp_min, x)
        return x

    # diffusive luminosity
    def Ld(DTYPE_t x, DTYPE_t t):
        # the following are equivalent
        #return x**2 * ET(x,0)*phi(t)/td
        return ET(x,t) * Rfunc(t)/R0 * x**2/td

    # optically thin luminosity
    def Lt(DTYPE_t x, DTYPE_t t):
        cdef DTYPE_t R = Rfunc(t)

        if R <= 0:
            return 0.0

        # need to find derivative of xp, which not possible explicitly.
        # find numerically
        cdef DTYPE_t dxpdt = scipy.misc.derivative(xp, t)

        # if the photosphere is expanding, the optically thin luminosity is zero
        if dxpdt > 0:
            return 0.0

        # with numerical differentiation
        cdef DTYPE_t luminosity = 4*np.pi * R0**3 * a_rad*T0**4 * phi(t) * (Rfunc(t)/R0)**(-1) * x**2 * psi(x) * (-dxpdt)

        # fast analytic version assuming pure scattering
        #cdef DTYPE_t luminosity = 4*np.pi * R0**3 * a_rad*T0**4 * phi(t) * x**2 * psi(x) * 2*(taup/taus0)*Rdot(t)/R0

        return luminosity


    # total luminosity, accounting for diffusive and thin
    def Ltotal(DTYPE_t t):
        # evaluate at the photosphere, or the at the minimum occurred
        # photosphere, if the bubble is turning optically thick again
        cdef DTYPE_t x = xthick(t)
        # get luminosities
        cdef DTYPE_t total = Ld(x, t) + Lt(x, t)

        return total

    # effective black body temperature and dilution factor of the photosphere
    def Teff(DTYPE_t t):
        cdef DTYPE_t Rphot
        # the effective temperature is evaluated using the _current_ value of
        # the photosphere, regardless of whether the region has been optically
        # thin before
        Rphot = Rfunc(t) * xp(t)
        if Rphot <= 0:
            return 0.0
        return ( Ltotal(t)/(4*np.pi*Rphot**2*sigma_sb) )**(1./4)

        return retval

    # optional support for possible diluted blackbody spectrum.
    # currently not used.
    def dilution(DTYPE_t t):
        # if dilution not used
        if 1:
            return 1.0

        # dilution is zero for R(t) < 0, though any value would suffice
        cdef DTYPE_t Rphot = Rfunc(t) * xp(t)
        cdef DTYPE_t Te = Teff(t)
        if Rphot <= 0:
            return 0.0
        if Te <= 0:
            # temperature is ~zero. the dilution doesn't really matter,
            # but returning zero is consistent
            return 0.0
        cdef DTYPE_t denom = 4*np.pi*Rphot**2 *sigma_sb * Te**4
        # BB dilution shouldn't exceed 1.0. in principle, this would
        # mean that the combined diffusive & escaped luminosity is
        # greater than the luminosity of the BB photosphere at the
        # given temperature. this isn't physically motivated.
        cdef DTYPE_t retval = dbl_max(Ltotal(t) / denom, 1.0)
        return retval


    # doppler boost at latitude
    def doppler(DTYPE_t beta, DTYPE_t theta):
        cdef DTYPE_t gamma = 1/np.sqrt(1-beta**2)
        return 1/(gamma * (1 - beta*np.cos(theta)))

    # relation between observation and emission times
    def emission_time(DTYPE_t t_obs, DTYPE_t theta):
        # this is correct to first order, i.e. for x_p ~ 1.
        if 0:
            return doppler(beta0, theta)/(1+z) * (t_obs - R0/c*(1-np.cos(theta)))

        # we can iterate to obtain a more accurate solution
        cdef DTYPE_t told = t_obs
        cdef DTYPE_t t = t_obs
        cdef DTYPE_t maxx = xp(t)
        cdef DTYPE_t beta = maxx*beta0
        cdef unsigned int i
        for i in xrange(4):
            t_old = t
            t = doppler(beta, theta)/(1+z) * (t_obs - R0/c*(1-np.cos(theta)))
            #print "rel change", np.abs((t-t_old)/(t+1e-30))
            maxx = xp(t)
            beta = maxx*beta0
        return t

    #
    # print initial and derived quantities

    # the integrand of the flux integral
    def integrand(DTYPE_t theta, DTYPE_t freq, DTYPE_t t_obs):
        cdef DTYPE_t evalt = emission_time(t_obs, theta)

        # if the emission time is outside the boundaries where
        # the model is valid, then to a very good
        # approximation the emitted intensity should be zero.
        if evalt < -th:
            return 0.0

        cdef DTYPE_t Te = Teff(evalt)
        cdef DTYPE_t dil = dilution(evalt)
        cdef DTYPE_t Rt = Rfunc(evalt)
        cdef DTYPE_t maxx = xp(evalt)
        #cdef DTYPE_t D = doppler(beta0, theta)
        cdef DTYPE_t D = doppler(maxx*beta0, theta)
        cdef DTYPE_t planck_B = planck(freq/D*(1+z), Te)

        #print "evalt", evalt, "Te", Te, "dil", dil, "Rt", Rt, "maxx", maxx

        if not np.isfinite(Te) or not np.isfinite(dil):
            return 0.0

        # Doppler boosted and shifted intensity, with dilution
        # factor
        Inu = D**3 * dil * planck_B

        # final value of integrand, as in the paper
        retval = Inu * (maxx*Rt)**2 * np.sin(2*theta)

        # the integrand is not numerically well defined when t
        # is too large or t < 0. the correct value exists and is
        # zero
        if not np.isfinite(retval):
            retval = 0.0
        return retval

    # integrated flux
    def relativistic_flux(DTYPE_t t_obs, DTYPE_t freq):
        # the constant part of the integral
        cdef DTYPE_t retval = np.pi*(1+z)*luminosity_distance**(-2)

        # loop over the times


        # theta-integration limit constrained by relativistic effects
        # XXX: note, we evaluate theta_max at time calculated for theta
        # = 0, this is correct to first order in x_p * beta
        cdef DTYPE_t evalt = emission_time(t_obs, 0)
        cdef DTYPE_t maxx = xp(evalt)
        cdef DTYPE_t maxtheta = np.arccos(maxx*beta0)
        cdef DTYPE_t integral, abserr

        # integrate over angles
        (integral, abserr) = integ.quad(integrand, 0, maxtheta, args=(freq, t_obs), epsrel=si_tolrel, epsabs=si_tolabs)
        return integral * retval

    # build result by looping over times
    cdef unsigned int j, k
    cdef unsigned int tmax = len(tssec)
    cdef unsigned int freqmax = len(spectrum_bins)
    cdef np.ndarray[DTYPE_t,ndim=2] spectra = np.zeros([tmax, freqmax], dtype=DTYPE)

    for j in range(tmax):
        for k in range(freqmax):
            spectra[j,k] = relativistic_flux(tssec[j], spectrum_bins[k])

    # calculate model quantities
    # N.B.: these are calculated in _rest frame_ of a parcel of the
    # expanding source.
    cdef np.ndarray[DTYPE_t,ndim=1] tes      = np.zeros([tmax], dtype=DTYPE)
    cdef np.ndarray[DTYPE_t,ndim=1] lums     = np.zeros([tmax], dtype=DTYPE)
    cdef np.ndarray[DTYPE_t,ndim=1] xes      = np.zeros([tmax], dtype=DTYPE)
    cdef np.ndarray[DTYPE_t,ndim=1] xss      = np.zeros([tmax], dtype=DTYPE)
    cdef np.ndarray[DTYPE_t,ndim=1] xps      = np.zeros([tmax], dtype=DTYPE)
    cdef np.ndarray[DTYPE_t,ndim=1] xthicks  = np.zeros([tmax], dtype=DTYPE)
    cdef np.ndarray[DTYPE_t,ndim=1] dlums    = np.zeros([tmax], dtype=DTYPE)
    cdef np.ndarray[DTYPE_t,ndim=1] tlums    = np.zeros([tmax], dtype=DTYPE)
    cdef np.ndarray[DTYPE_t,ndim=1] phis     = np.zeros([tmax], dtype=DTYPE)
    cdef np.ndarray[DTYPE_t,ndim=1] expfacs  = np.zeros([tmax], dtype=DTYPE)
    cdef np.ndarray[DTYPE_t,ndim=1] Teffs    = np.zeros([tmax], dtype=DTYPE)
    cdef np.ndarray[DTYPE_t,ndim=1] Ts       = np.zeros([tmax], dtype=DTYPE)
    cdef np.ndarray[DTYPE_t,ndim=1] Rs       = np.zeros([tmax], dtype=DTYPE)

    cdef DTYPE_t t
    for j in range(tmax):
        # user may want luminosity and other quantities evaluated either
        # in the rest frame of the observer or of the expanding
        # photosphere. the latter option corresponds to the classical
        # A80 model. for now, we return everything at observed times.

        # in rest frame of observer, quantities evaluated for the gas at
        # theta = 0
        if use_obs_frame:
            t = emission_time(tssec[j], 0)
        else:
            # in rest frame of gas
            t = tssec[j]
        # approximation good for small beta, which is required for model
        # validity in the first place.
        #t = (1+beta0)/(1+z) * tssec[j]

        #
        # evaluate the quantities

        # times
        tes[j] = t
        # radii
        Rs[j] = Rfunc(t)
        # total luminosity
        lums[j]    = Ltotal(t)
        # relative photosphere radius by shibata effective depth
        xes[j] = xe(t)
        # relative photosphere radius by Thomson optical depth
        xss[j] = xs(t)
        # relative photosphere radius
        xps[j] = xp(t)
        # optically thick region
        xthicks[j] = xthick(t)
        # diffusive and optically thin luminosities
        dlums[j]    = Ld(xthicks[j],t)
        # diffusive and optically thin luminosities
        tlums[j]    = Lt(xthicks[j],t)
        # the phi function
        phis[j]    = phi(t)
        # expansion factors
        expfacs[j] = Rfunc(t)/R0
        # effective temperatures
        Teffs[j]      = Teff(t)
        # internal temperatures
        Ts[j] = T0 * phis[j]**(1./4) * expfacs[j]**(-1)


    return {
            'spectra' : spectra,
            'L'       : lums,
            'xe'      : xes,
            'xs'      : xss,
            'xp'      : xps,
            'Ld'      : dlums,
            'Lt'      : tlums,
            'phi'     : phis,
            'xi'      : expfacs,
            'Teff'    : Teffs,
            'T'       : Ts,
            'R'       : Rs,
            'time'   : tssec,
            'output_time'   : tes,

            # input parameters & derived quantities
            'z'       : z,
            'DL'      : luminosity_distance,
            'R0'      : R0,
            'T0'      : T0,
            'rho0'    : rho0,
            'beta0'   : beta0,
            'taup'    : taup,
            'alpha'   : alpha,
            'M0'      : M0,
            'td'      : td,
            'th'      : th,
            'taus0'   : taus0,
            'taua0'   : taua0,
            'ET0'     : ET(1.0,0.0)
            }

