import matplotlib.pyplot as plt
import numpy as np

# for directly importing Cython files, pyximport must be used
import pyximport; pyximport.install()
import burst_model

# set parameters of the binary and the accretion disk
z = 0.5
lum_dist = 7.953e27 # in cm, using Planck2013 cosmology
M8  = 1.0
r10 = 1.0
h15 = 1.0
n14 = 1.0
incp = 1.0
# set beta equal to impact velocity
beta = 1/np.sqrt(10.0)*r10**(-1./2)

# convert to initial outflow values
(T0, R0, rho0) = burst_model.calculate_impact(r10, h15, n14, M8, incp)

# evaluate at 100 times divided evenly over 0.5 years
times = np.linspace(0, 0.5, 100)

# get observed flux at frequency corresponding to wavelength of 500 nm
freq = 6e14

# run model
params = (R0, T0, rho0, beta)
model = burst_model.model(times, z, lum_dist, np.array([freq]), *params)
plt.plot(times, model['spectra'])
plt.show()
